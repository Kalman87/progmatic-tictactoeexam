/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import com.progmatic.tictactoeexam.interfaces.Player;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kalman
 */
public class MyPlayer1 implements Player {

    protected PlayerType ply;

    public MyPlayer1(PlayerType ply) {
        this.ply = ply;
    }

    @Override
    public Cell nextMove(Board b) {
        MyBoard t = new MyBoard();
        PlayerType pt = null;
        Cell c = null;
        for (int i = 0; i < t.table.length; i++) {
            for (int j = 0; j < t.table[i].length; j++) {
                if (t.emptyCells().size() == 0) {
                    return c;
                } else {
                    try {
                        pt = t.getCell(i, j);
                    } catch (CellException ex) {
                        Logger.getLogger(MyPlayer1.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (pt == PlayerType.EMPTY) {
                        try {
                            t.put(Cell(i, j, ply));
                            c = Cell(i, j, ply);
                        } catch (CellException ex) {
                            Logger.getLogger(MyPlayer1.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }
        return c;
    }

    @Override
    public PlayerType getMyType() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Cell Cell(int i, int j, PlayerType pt) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kalman
 */
public class MyBoard implements com.progmatic.tictactoeexam.interfaces.Board {

    PlayerType[][] table = new PlayerType[3][3];

    public MyBoard() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                table[i][j] = PlayerType.EMPTY;
            }
        }
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        PlayerType pt;
        pt = table[colIdx][rowIdx];

        if (rowIdx > 2 || colIdx > 2 || rowIdx < 0 || colIdx < 0) {
            CellException incorrectIndex = new CellException();
            throw incorrectIndex;
        }
        return pt;
    }

    @Override
    public void put(Cell cell) throws CellException {
        int row = cell.getRow();
        int col = cell.getCol();

        if (row > table.length - 1 || col > table.length - 1 || row < 0 || col < 0) {
            CellException incorrectIndex = new CellException();
            throw incorrectIndex;
        }

        if (table[col][row].equals(PlayerType.EMPTY)) {
            table[col][row] = cell.getCellsPlayer();
        } else {
            CellException fullCell = new CellException();
            throw fullCell;
        }
    }

    @Override
    public boolean hasWon(PlayerType p) {
        boolean hasWon = false;

        for (int i = 0; i < table.length; i++) {
            int sor = 0;
            int oszlop = 0;
            int atlo = 0;
            int katlo = 0;
            for (int j = 0; j < table[i].length; j++) {
                int z = table.length - 1 - j;
                if (table[i][j].equals(p)) {
                    sor++;
                }

                if (table[j][i].equals(p)) {
                    oszlop++;
                }

                if (table[j][j].equals(p)) {
                    atlo++;
                }

                if (table[j][z].equals(p)) {
                    katlo++;
                }
            }

            if (sor == table.length
                    || oszlop == table.length
                    || atlo == table.length
                    || katlo == table.length) {

                hasWon = true;
                return hasWon;
            }
        }
        return hasWon;
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> empty = new ArrayList<>();
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                if (table[i][j].equals(PlayerType.EMPTY)) {
                    empty.add(new Cell(i, j));
                }
            }
        }
        return empty;
    }

}
